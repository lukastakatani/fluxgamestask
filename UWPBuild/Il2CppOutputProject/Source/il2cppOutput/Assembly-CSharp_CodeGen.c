﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void Life::TakesLife(System.Int32)
extern void Life_TakesLife_m432769A0DBB59D454784B3402F19D358B66CBDBD (void);
// 0x00000002 System.Void Life::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void Life_OnTriggerEnter2D_mB5E33226D7DB91144ACC54786FDF08388B017178 (void);
// 0x00000003 System.Void Life::.ctor()
extern void Life__ctor_m7EA475E113F8E58CDBD7F64DE7C3378510DD7926 (void);
// 0x00000004 System.Void ControlGame::Start()
extern void ControlGame_Start_m41EDFA823653EE35B75C9E396B158F9C47330225 (void);
// 0x00000005 System.Void ControlGame::Update()
extern void ControlGame_Update_m75CA34A327D9AE3F5BE315C068BB2D3946985B50 (void);
// 0x00000006 System.Void ControlGame::PauseMusic()
extern void ControlGame_PauseMusic_m16FCABBE2805B4A9C3BFF4D691919B61104381E3 (void);
// 0x00000007 System.Void ControlGame::ContinueMusic()
extern void ControlGame_ContinueMusic_mF0FCA6D12DB9684795658C1C4FF1BC3ACDDA4EEE (void);
// 0x00000008 System.Void ControlGame::EnablePauseWindow()
extern void ControlGame_EnablePauseWindow_mC87BB414060C0AD7214F136AEE360212385932A2 (void);
// 0x00000009 System.Void ControlGame::DisablePauseWindow()
extern void ControlGame_DisablePauseWindow_mF81275A2C40F059BF45495640DFC05A2E629BD4A (void);
// 0x0000000A System.Void ControlGame::LevelPassed()
extern void ControlGame_LevelPassed_m05B6235DD8F835987CA547397293A735BB6F6534 (void);
// 0x0000000B System.Void ControlGame::GameOver()
extern void ControlGame_GameOver_m1E16954A2EB5306CA5D7959354BD4728C0321A13 (void);
// 0x0000000C System.Void ControlGame::Clear()
extern void ControlGame_Clear_m0681179893AC0309B4E59F5EA1033BE470C10074 (void);
// 0x0000000D System.Void ControlGame::.ctor()
extern void ControlGame__ctor_mBB7334A9B707AA5AB4EC6F7C853BE92433BB4D4D (void);
// 0x0000000E System.Void ControlStart::Start()
extern void ControlStart_Start_m201D1963F24351E276FC706916096DE032376F51 (void);
// 0x0000000F System.Void ControlStart::StartClick()
extern void ControlStart_StartClick_m4F919D6E668B781C8C5DC0EB2EB21611D882EFAB (void);
// 0x00000010 System.Void ControlStart::ContinueClick()
extern void ControlStart_ContinueClick_mBD000E184A0B18DFBA59C7536536E203CD989867 (void);
// 0x00000011 System.Void ControlStart::Quit()
extern void ControlStart_Quit_mD819D0CAD1831F9E534C96E210F5FFFF9FDB1747 (void);
// 0x00000012 System.Void ControlStart::.ctor()
extern void ControlStart__ctor_mB1F89FE5B8F09B1F79FC79EA5BFDBDCEBEF790EA (void);
// 0x00000013 System.Void EnemyControl::Start()
extern void EnemyControl_Start_mB2709A00C33F64E801FB336D9224A48BAEDF451F (void);
// 0x00000014 System.Collections.IEnumerator EnemyControl::Process()
extern void EnemyControl_Process_m3026B4978A8349BD60037F2B25AFA4539B70FB7B (void);
// 0x00000015 System.Void EnemyControl::EnemyCreate()
extern void EnemyControl_EnemyCreate_m1DBC2B3B2C19C6C6D410EAB1360BB814DD12FD3E (void);
// 0x00000016 System.Collections.IEnumerator EnemyControl::CallBoss()
extern void EnemyControl_CallBoss_mEE1F75478CABCE55BC6A16FA00D0F843E7A58101 (void);
// 0x00000017 System.Void EnemyControl::.ctor()
extern void EnemyControl__ctor_mE15FA6B97AEFE883D943CC0D237897F435BC34BF (void);
// 0x00000018 System.Void EnemyControl/<Process>d__4::.ctor(System.Int32)
extern void U3CProcessU3Ed__4__ctor_mA6DCD4EAD0188CA4C4AD0A5EF0D2A9FC2F8ADC15 (void);
// 0x00000019 System.Void EnemyControl/<Process>d__4::System.IDisposable.Dispose()
extern void U3CProcessU3Ed__4_System_IDisposable_Dispose_m382FC3C37C2241AC4E64B8D0EC2C821F916C3DE2 (void);
// 0x0000001A System.Boolean EnemyControl/<Process>d__4::MoveNext()
extern void U3CProcessU3Ed__4_MoveNext_m3A08B74C2B6D96C327D949D051A8EA8D5B423DDC (void);
// 0x0000001B System.Object EnemyControl/<Process>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CProcessU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m277414C234EE05878133959D21839B5949AC295B (void);
// 0x0000001C System.Void EnemyControl/<Process>d__4::System.Collections.IEnumerator.Reset()
extern void U3CProcessU3Ed__4_System_Collections_IEnumerator_Reset_mE513AF299BB9BF436B27408FF4C0558A73F4676D (void);
// 0x0000001D System.Object EnemyControl/<Process>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CProcessU3Ed__4_System_Collections_IEnumerator_get_Current_m158EF3608628631AC8F9A0A6CE2568AEB6566996 (void);
// 0x0000001E System.Void EnemyControl/<CallBoss>d__6::.ctor(System.Int32)
extern void U3CCallBossU3Ed__6__ctor_mDA19F5EC5865BAF920AD4E709D8ADE95E743F363 (void);
// 0x0000001F System.Void EnemyControl/<CallBoss>d__6::System.IDisposable.Dispose()
extern void U3CCallBossU3Ed__6_System_IDisposable_Dispose_m68D5010133251BF57F27F51D1BB928B1E4C8DDD9 (void);
// 0x00000020 System.Boolean EnemyControl/<CallBoss>d__6::MoveNext()
extern void U3CCallBossU3Ed__6_MoveNext_mDF3544E995D32004659624A65FB35CCD76C0B909 (void);
// 0x00000021 System.Object EnemyControl/<CallBoss>d__6::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CCallBossU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2297F4117C95DDE24CFD237A355224230FA00BD9 (void);
// 0x00000022 System.Void EnemyControl/<CallBoss>d__6::System.Collections.IEnumerator.Reset()
extern void U3CCallBossU3Ed__6_System_Collections_IEnumerator_Reset_mAB176BC3F3DE4AD36A0EAF25D234733D5CA47B3D (void);
// 0x00000023 System.Object EnemyControl/<CallBoss>d__6::System.Collections.IEnumerator.get_Current()
extern void U3CCallBossU3Ed__6_System_Collections_IEnumerator_get_Current_mB515EAE6DED7E678D67D764943739ADEF1C84D27 (void);
// 0x00000024 System.Void RecordControl::Start()
extern void RecordControl_Start_m8F7A53F239DE398E63E55AA2A9EF390392EB516F (void);
// 0x00000025 System.Void RecordControl::UpdateName()
extern void RecordControl_UpdateName_m3C7C5ABFF677519820B6F281AF99A768064360DC (void);
// 0x00000026 System.Void RecordControl::.ctor()
extern void RecordControl__ctor_m8B9BD294AC3D1EDA3869E09F7EFC58F7B97304C4 (void);
// 0x00000027 System.Void AvoidPlayerShoot::Update()
extern void AvoidPlayerShoot_Update_m9F04CC7942F3564D7D6D767688B1085737082DE0 (void);
// 0x00000028 System.Void AvoidPlayerShoot::OnTriggerStay2D(UnityEngine.Collider2D)
extern void AvoidPlayerShoot_OnTriggerStay2D_m11A3D1907CABECF03E64055F6D74EAC320B8F93A (void);
// 0x00000029 System.Collections.IEnumerator AvoidPlayerShoot::StopMovingRightRoutine()
extern void AvoidPlayerShoot_StopMovingRightRoutine_m1270F8255ABDE3EF3F9345B44D7199C5A2DD1E8C (void);
// 0x0000002A System.Void AvoidPlayerShoot::.ctor()
extern void AvoidPlayerShoot__ctor_m2F72033E8F0B0C9A69210A0239507FAA88D97B05 (void);
// 0x0000002B System.Void AvoidPlayerShoot/<StopMovingRightRoutine>d__4::.ctor(System.Int32)
extern void U3CStopMovingRightRoutineU3Ed__4__ctor_mC9E973AD034D3BDAF7C2A6D99C47B0ABF62B1A27 (void);
// 0x0000002C System.Void AvoidPlayerShoot/<StopMovingRightRoutine>d__4::System.IDisposable.Dispose()
extern void U3CStopMovingRightRoutineU3Ed__4_System_IDisposable_Dispose_mEB6B7FE7825B7B23E4700FE655EC0119ACF7CF81 (void);
// 0x0000002D System.Boolean AvoidPlayerShoot/<StopMovingRightRoutine>d__4::MoveNext()
extern void U3CStopMovingRightRoutineU3Ed__4_MoveNext_mB25589E8D5E9A567F1326D8113BB87EF4D0EB2B1 (void);
// 0x0000002E System.Object AvoidPlayerShoot/<StopMovingRightRoutine>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStopMovingRightRoutineU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6FD7E48346DDC6DB25D8000EBA117799B428AD73 (void);
// 0x0000002F System.Void AvoidPlayerShoot/<StopMovingRightRoutine>d__4::System.Collections.IEnumerator.Reset()
extern void U3CStopMovingRightRoutineU3Ed__4_System_Collections_IEnumerator_Reset_m33895CB0E0464291042ECB278CF9FB89845E74E5 (void);
// 0x00000030 System.Object AvoidPlayerShoot/<StopMovingRightRoutine>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CStopMovingRightRoutineU3Ed__4_System_Collections_IEnumerator_get_Current_mFDFA2C26A33A873B1930730FC06967FCA3100825 (void);
// 0x00000031 System.Void Boss2::Start()
extern void Boss2_Start_mF5292CA924E8DB443334DB78CCD7201F768E15C8 (void);
// 0x00000032 System.Collections.IEnumerator Boss2::ShowParts()
extern void Boss2_ShowParts_mCDC314F0FBDB8D208165FAAE3F8F4D358F7FD315 (void);
// 0x00000033 System.Void Boss2::Update()
extern void Boss2_Update_m8CE583E0DAC32C0A374F69542AD8C054754C0C14 (void);
// 0x00000034 System.Collections.IEnumerator Boss2::Shot()
extern void Boss2_Shot_m35B8B00F7AEF10F996EF1D55BEF3575E5BAFF3D6 (void);
// 0x00000035 UnityEngine.GameObject Boss2::GetOneActive()
extern void Boss2_GetOneActive_m41CEB8C5D4F936E741F9C9F684F40EAE7D3DADA3 (void);
// 0x00000036 System.Void Boss2::KillMe(UnityEngine.GameObject)
extern void Boss2_KillMe_mDE3ED52B5D597F8CEB197D98E1F15E9334C71285 (void);
// 0x00000037 System.Void Boss2::.ctor()
extern void Boss2__ctor_mAF0CEE4D38FEAFC1DE100B0F2E0701E06B14991F (void);
// 0x00000038 System.Void Boss2/<ShowParts>d__5::.ctor(System.Int32)
extern void U3CShowPartsU3Ed__5__ctor_mBDD0E8898D91E138B7042D22F0A5E1456EA501C8 (void);
// 0x00000039 System.Void Boss2/<ShowParts>d__5::System.IDisposable.Dispose()
extern void U3CShowPartsU3Ed__5_System_IDisposable_Dispose_m0AC2D2E39174DA2D173BDA8759091EEE1D71F146 (void);
// 0x0000003A System.Boolean Boss2/<ShowParts>d__5::MoveNext()
extern void U3CShowPartsU3Ed__5_MoveNext_m0B40D4DCC0A1EBAC248878B225A64D4925E31704 (void);
// 0x0000003B System.Object Boss2/<ShowParts>d__5::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CShowPartsU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4F4521C1346516C77ED9C7B550671F928987CE41 (void);
// 0x0000003C System.Void Boss2/<ShowParts>d__5::System.Collections.IEnumerator.Reset()
extern void U3CShowPartsU3Ed__5_System_Collections_IEnumerator_Reset_m0468FE7B0426062B26C538D23C7C5FF8CC38570D (void);
// 0x0000003D System.Object Boss2/<ShowParts>d__5::System.Collections.IEnumerator.get_Current()
extern void U3CShowPartsU3Ed__5_System_Collections_IEnumerator_get_Current_m20F3D7FA705381FA5AA9C2B5C037BDCB2BC2128A (void);
// 0x0000003E System.Void Boss2/<Shot>d__7::.ctor(System.Int32)
extern void U3CShotU3Ed__7__ctor_mE5DADDE0420441F86985C44BD718DD08A4AD334C (void);
// 0x0000003F System.Void Boss2/<Shot>d__7::System.IDisposable.Dispose()
extern void U3CShotU3Ed__7_System_IDisposable_Dispose_m8F588491D89FF21288555692FE3A90E505E341F7 (void);
// 0x00000040 System.Boolean Boss2/<Shot>d__7::MoveNext()
extern void U3CShotU3Ed__7_MoveNext_m6010D4868EF31AE0774066824F01233F5F37D4AE (void);
// 0x00000041 System.Object Boss2/<Shot>d__7::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CShotU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m12B604A9AC558372662A0FFD180A325ACDD91EA6 (void);
// 0x00000042 System.Void Boss2/<Shot>d__7::System.Collections.IEnumerator.Reset()
extern void U3CShotU3Ed__7_System_Collections_IEnumerator_Reset_mE289E17958922FDDCC8DE7C1FA61940025274E62 (void);
// 0x00000043 System.Object Boss2/<Shot>d__7::System.Collections.IEnumerator.get_Current()
extern void U3CShotU3Ed__7_System_Collections_IEnumerator_get_Current_m935EF7A6B9356B060AA9DFD3E78AA608C890BC25 (void);
// 0x00000044 System.Void Boss3::Start()
extern void Boss3_Start_m025FF1DBDC22B6304FBDBAD41E2A028B5BCF4E2C (void);
// 0x00000045 System.Collections.IEnumerator Boss3::ShowParts(System.Boolean)
extern void Boss3_ShowParts_m237CC1FA5AE12E3DC8B0F2E864759C88F37E1EC6 (void);
// 0x00000046 System.Collections.IEnumerator Boss3::AttackNow()
extern void Boss3_AttackNow_m751CAFCD3CD70F4748E1BE4A61688B418BF83420 (void);
// 0x00000047 System.Collections.IEnumerator Boss3::Attack(UnityEngine.UI.Image)
extern void Boss3_Attack_mBC827254E14F721046A48C74FBDCE480D7EB8006 (void);
// 0x00000048 UnityEngine.GameObject Boss3::GetOneActive()
extern void Boss3_GetOneActive_mC454EE4919EB35E3CADF35A2C047B267CBB5754F (void);
// 0x00000049 System.Void Boss3::KillMe(UnityEngine.GameObject)
extern void Boss3_KillMe_m485F8403E9462C6F16E4A1E1346373AE8AE15029 (void);
// 0x0000004A System.Void Boss3::.ctor()
extern void Boss3__ctor_m643E6AD2933C0D491F1706E2F4CA3BA2F3820306 (void);
// 0x0000004B System.Void Boss3/<ShowParts>d__5::.ctor(System.Int32)
extern void U3CShowPartsU3Ed__5__ctor_mE02D1048C34075267CA9F8A9AAA9BAA01A251D2D (void);
// 0x0000004C System.Void Boss3/<ShowParts>d__5::System.IDisposable.Dispose()
extern void U3CShowPartsU3Ed__5_System_IDisposable_Dispose_mFFAB94361C876791A1720E45B41DDE4920D6E962 (void);
// 0x0000004D System.Boolean Boss3/<ShowParts>d__5::MoveNext()
extern void U3CShowPartsU3Ed__5_MoveNext_mCD5696204CDF2A5ECBC56EF51E055A070AC9FB77 (void);
// 0x0000004E System.Object Boss3/<ShowParts>d__5::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CShowPartsU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m46AC63FF765EC7ABFD50ABD1A0EC06666CCE0B0F (void);
// 0x0000004F System.Void Boss3/<ShowParts>d__5::System.Collections.IEnumerator.Reset()
extern void U3CShowPartsU3Ed__5_System_Collections_IEnumerator_Reset_m6B70E7B67CF4CDCB8A4933138894ADD37C944524 (void);
// 0x00000050 System.Object Boss3/<ShowParts>d__5::System.Collections.IEnumerator.get_Current()
extern void U3CShowPartsU3Ed__5_System_Collections_IEnumerator_get_Current_mE6AF45BEC418417DB98C019E25CDB0370CB836FF (void);
// 0x00000051 System.Void Boss3/<AttackNow>d__6::.ctor(System.Int32)
extern void U3CAttackNowU3Ed__6__ctor_m807B06176433FA7EB7E91A0C957DA1F3903A1BB9 (void);
// 0x00000052 System.Void Boss3/<AttackNow>d__6::System.IDisposable.Dispose()
extern void U3CAttackNowU3Ed__6_System_IDisposable_Dispose_m084196477A3C10B1664186E740CEE59FD3B30521 (void);
// 0x00000053 System.Boolean Boss3/<AttackNow>d__6::MoveNext()
extern void U3CAttackNowU3Ed__6_MoveNext_mEA7C3F8A7C4FA8E6D4387CE9F9A6709AFEB394E7 (void);
// 0x00000054 System.Object Boss3/<AttackNow>d__6::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAttackNowU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m340352050CC94EAE9A072030CD314DA56DD329AA (void);
// 0x00000055 System.Void Boss3/<AttackNow>d__6::System.Collections.IEnumerator.Reset()
extern void U3CAttackNowU3Ed__6_System_Collections_IEnumerator_Reset_m9E1C7C0E00159B453D194492ECF021B8BB7CD45A (void);
// 0x00000056 System.Object Boss3/<AttackNow>d__6::System.Collections.IEnumerator.get_Current()
extern void U3CAttackNowU3Ed__6_System_Collections_IEnumerator_get_Current_m574DD65751CE17C59EB1B1B4A52EC305243484D0 (void);
// 0x00000057 System.Void Boss3/<Attack>d__7::.ctor(System.Int32)
extern void U3CAttackU3Ed__7__ctor_m1E3D32EDA81B13F2EE38F2859D3B2C4F28E9D327 (void);
// 0x00000058 System.Void Boss3/<Attack>d__7::System.IDisposable.Dispose()
extern void U3CAttackU3Ed__7_System_IDisposable_Dispose_m6A015A6CA5CCBB4708004CC3651BB9B1D3725CFF (void);
// 0x00000059 System.Boolean Boss3/<Attack>d__7::MoveNext()
extern void U3CAttackU3Ed__7_MoveNext_mA361D488087872EB4D071C00B7A2E97B943ED301 (void);
// 0x0000005A System.Object Boss3/<Attack>d__7::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAttackU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF28BAAACFCDE294FEDE76B40F09FF75F8FC16E52 (void);
// 0x0000005B System.Void Boss3/<Attack>d__7::System.Collections.IEnumerator.Reset()
extern void U3CAttackU3Ed__7_System_Collections_IEnumerator_Reset_m5A5C9CB773069AF2AEB75EA9A0A398A9459D72B7 (void);
// 0x0000005C System.Object Boss3/<Attack>d__7::System.Collections.IEnumerator.get_Current()
extern void U3CAttackU3Ed__7_System_Collections_IEnumerator_get_Current_m52CBBA00FF612CAD09F60DF816EAC8E2A237414F (void);
// 0x0000005D System.Void Enemy::Start()
extern void Enemy_Start_m9FA35B427F2B9FDFD390E9812C2556775C62CB02 (void);
// 0x0000005E System.Void Enemy::Update()
extern void Enemy_Update_mA01EE7AF5D3B97687752E9D22BECB4A3E13F8FD2 (void);
// 0x0000005F System.Void Enemy::OnCollisionEnter2D(UnityEngine.Collision2D)
extern void Enemy_OnCollisionEnter2D_m47EAF0D1D60EC5BF3953975EA6BF15189DF82A12 (void);
// 0x00000060 System.Void Enemy::MyDeath()
extern void Enemy_MyDeath_mA4085ED4BD10E6B3EDEE186807D3C8FB762A4CC8 (void);
// 0x00000061 System.Collections.IEnumerator Enemy::KillMe()
extern void Enemy_KillMe_m24D0CC29C673187902BFD4554072865ADF3F2EB5 (void);
// 0x00000062 System.Void Enemy::Create(System.Int32)
extern void Enemy_Create_m66ACA885B980722A913D038190E87E6AFFA55759 (void);
// 0x00000063 System.Collections.IEnumerator Enemy::Shoot()
extern void Enemy_Shoot_m9698649D8FCE4A907B5EBAA30105E3DF881FB767 (void);
// 0x00000064 System.Void Enemy::EndBoss()
extern void Enemy_EndBoss_mA358579DEF2D3DAF68CD80B9AD4C96C1981554DA (void);
// 0x00000065 System.Void Enemy::PStick()
extern void Enemy_PStick_m5BAC91E00684770270D3B5164328B2EF020D9173 (void);
// 0x00000066 System.Void Enemy::.ctor()
extern void Enemy__ctor_m3C82F8269DE4132408E15B523907244771640734 (void);
// 0x00000067 System.Void Enemy/<KillMe>d__10::.ctor(System.Int32)
extern void U3CKillMeU3Ed__10__ctor_mCBE3F42CF5E6BEB5E774F037B0444C559C24EC4C (void);
// 0x00000068 System.Void Enemy/<KillMe>d__10::System.IDisposable.Dispose()
extern void U3CKillMeU3Ed__10_System_IDisposable_Dispose_m2DB73B1C1F038ACD13017517E648F7BC34FE75A7 (void);
// 0x00000069 System.Boolean Enemy/<KillMe>d__10::MoveNext()
extern void U3CKillMeU3Ed__10_MoveNext_m2B827634D8CEADE16F1ABA524332C1D91AAEEA08 (void);
// 0x0000006A System.Object Enemy/<KillMe>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CKillMeU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB162D3E0999042813FBE550CD7FD7926C498BF3C (void);
// 0x0000006B System.Void Enemy/<KillMe>d__10::System.Collections.IEnumerator.Reset()
extern void U3CKillMeU3Ed__10_System_Collections_IEnumerator_Reset_m144E9C6610E020F7905EF9E9E55E6D769047778C (void);
// 0x0000006C System.Object Enemy/<KillMe>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CKillMeU3Ed__10_System_Collections_IEnumerator_get_Current_m89F0A6CE9070973A816212BD6572DA6DC9D9F303 (void);
// 0x0000006D System.Void Enemy/<Shoot>d__12::.ctor(System.Int32)
extern void U3CShootU3Ed__12__ctor_m4884E34148F61D16DCBE9E6C9E29CF766636E036 (void);
// 0x0000006E System.Void Enemy/<Shoot>d__12::System.IDisposable.Dispose()
extern void U3CShootU3Ed__12_System_IDisposable_Dispose_m9E7ADDC293233521A708C4B1114000B969F4F6E4 (void);
// 0x0000006F System.Boolean Enemy/<Shoot>d__12::MoveNext()
extern void U3CShootU3Ed__12_MoveNext_mD258E3C08A9B604CFA4F85EFA5B6E32189C9EC55 (void);
// 0x00000070 System.Object Enemy/<Shoot>d__12::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CShootU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA3E2421A685652897F0F5F6D05DA25B8ACDEE5DD (void);
// 0x00000071 System.Void Enemy/<Shoot>d__12::System.Collections.IEnumerator.Reset()
extern void U3CShootU3Ed__12_System_Collections_IEnumerator_Reset_m5C552696876E029CE03DD55B746CD7422330D2DF (void);
// 0x00000072 System.Object Enemy/<Shoot>d__12::System.Collections.IEnumerator.get_Current()
extern void U3CShootU3Ed__12_System_Collections_IEnumerator_get_Current_m9D53A9EAB026124E6E8AF1E6BC87C3375A604866 (void);
// 0x00000073 System.Void ItemDrop::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void ItemDrop_OnTriggerEnter2D_m1A1B2CBD8DABD7C2C8C666248C9E7B9A56F022A7 (void);
// 0x00000074 System.Void ItemDrop::.ctor()
extern void ItemDrop__ctor_m5D826B2329C00417D6FB12D4C40DE3EB0DB55AF2 (void);
// 0x00000075 System.Void GameEvent::Raise()
extern void GameEvent_Raise_m9B5D84B86942CF2BFF6B064CFBE740D14AC41169 (void);
// 0x00000076 System.Void GameEvent::RegisterListener(GameEventListener)
extern void GameEvent_RegisterListener_mCF8253B702F145F689525B4DFD266F14259941DE (void);
// 0x00000077 System.Void GameEvent::UnregisterListener(GameEventListener)
extern void GameEvent_UnregisterListener_mA64E767AE34F712A1D506DC25037A57302DCAF59 (void);
// 0x00000078 System.Void GameEvent::.ctor()
extern void GameEvent__ctor_m8337DB39CF91F554DDF9F08F43DD8093CDF3D362 (void);
// 0x00000079 System.Void GameEventListener::OnEnable()
extern void GameEventListener_OnEnable_mED6474898A6BDA74E5901EC4F92179E5A743EABF (void);
// 0x0000007A System.Void GameEventListener::OnDisable()
extern void GameEventListener_OnDisable_m294B8AD260F889DC6315ABBC3708388BC69D0FB2 (void);
// 0x0000007B System.Void GameEventListener::OnEventRaised()
extern void GameEventListener_OnEventRaised_m64F601143F91C20869FE7D570F8C4530606AE873 (void);
// 0x0000007C System.Void GameEventListener::.ctor()
extern void GameEventListener__ctor_m59096273CAC9367011CA8BFD14CCBA3E3A4CC8AD (void);
// 0x0000007D System.Void GameSave::Awake()
extern void GameSave_Awake_m8FDE53993AECC9CA915BFD62E86F10EA2AB272FC (void);
// 0x0000007E System.Void GameSave::Start()
extern void GameSave_Start_m0A5DC52CFF75277784AB9A1E9CE44532AB0023A1 (void);
// 0x0000007F System.Void GameSave::LoadSaveData()
extern void GameSave_LoadSaveData_mA0E87179E2A403812A58E5BB704981C7EC2B3E17 (void);
// 0x00000080 System.Void GameSave::.ctor()
extern void GameSave__ctor_mC4C7321C5218614930AB431A5E1254608C3F4EA8 (void);
// 0x00000081 System.Void GameSaveData::.ctor(System.String,System.Int32)
extern void GameSaveData__ctor_m558C28225F9028417ADA7AC2A15D1702A6F2788C (void);
// 0x00000082 System.Void GameSaveDatas::.ctor()
extern void GameSaveDatas__ctor_m2F11588F7657D30EBEB87E30ECE51BE6797D92DB (void);
// 0x00000083 System.Void FollowPlayer::Awake()
extern void FollowPlayer_Awake_mC670530149DD0AF283FBD985226E690947E9BA2B (void);
// 0x00000084 System.Void FollowPlayer::LateUpdate()
extern void FollowPlayer_LateUpdate_m15E369E8794A1195493D5B2C5E0183739DCB8EC0 (void);
// 0x00000085 System.Void FollowPlayer::.ctor()
extern void FollowPlayer__ctor_mE71062263690B516F460CF232D5144C1C85017AA (void);
// 0x00000086 System.Void CallScene::Call(System.String)
extern void CallScene_Call_m8CDF9D77505FAEDF29068B890862D09654434CCE (void);
// 0x00000087 System.Void CallScene::.ctor()
extern void CallScene__ctor_m6B52AD5FB87324D006A0BFF36D122D8E5096AE9C (void);
// 0x00000088 System.Void DestroyTime::Start()
extern void DestroyTime_Start_m8C0A193A37746E7A2A2C617E998A31E724373497 (void);
// 0x00000089 System.Collections.IEnumerator DestroyTime::CallDestroy()
extern void DestroyTime_CallDestroy_m61D5F2B05283C2474DC66285AC88FDA79B8E1BD6 (void);
// 0x0000008A System.Void DestroyTime::.ctor()
extern void DestroyTime__ctor_mAB5E312F1EAAD1B3B69CC64F2B95B607F8EE4F88 (void);
// 0x0000008B System.Void DestroyTime/<CallDestroy>d__2::.ctor(System.Int32)
extern void U3CCallDestroyU3Ed__2__ctor_m7A7A8DC06CB12FA979DC587DA59CD00B21710FAF (void);
// 0x0000008C System.Void DestroyTime/<CallDestroy>d__2::System.IDisposable.Dispose()
extern void U3CCallDestroyU3Ed__2_System_IDisposable_Dispose_m93E09EB5836B606D9F59C3CB667BDD2C8F1215BD (void);
// 0x0000008D System.Boolean DestroyTime/<CallDestroy>d__2::MoveNext()
extern void U3CCallDestroyU3Ed__2_MoveNext_mCDEF6B443D6ED1BA89399F05AEC81423D64C107B (void);
// 0x0000008E System.Object DestroyTime/<CallDestroy>d__2::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CCallDestroyU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA97E34B5CF7FEDA8B2A9EFBCE21A197CE51A2BB2 (void);
// 0x0000008F System.Void DestroyTime/<CallDestroy>d__2::System.Collections.IEnumerator.Reset()
extern void U3CCallDestroyU3Ed__2_System_Collections_IEnumerator_Reset_mEAC272124FCDF01C2C344D034F9CBA6F076E2EF9 (void);
// 0x00000090 System.Object DestroyTime/<CallDestroy>d__2::System.Collections.IEnumerator.get_Current()
extern void U3CCallDestroyU3Ed__2_System_Collections_IEnumerator_get_Current_m89A18098EC6562CFAFAD044268F750C304307BA0 (void);
// 0x00000091 System.Void Explosion::Start()
extern void Explosion_Start_m519BD45EC393F52D86FB64B10889D5CBADCF4C22 (void);
// 0x00000092 System.Collections.IEnumerator Explosion::Explode()
extern void Explosion_Explode_m0586F5F99D95A44520E522FA9CF6256DCB7FC258 (void);
// 0x00000093 System.Void Explosion::.ctor()
extern void Explosion__ctor_m1400515C43124E852380BB8283E15042AF0A5094 (void);
// 0x00000094 System.Void Explosion/<Explode>d__3::.ctor(System.Int32)
extern void U3CExplodeU3Ed__3__ctor_mA6402CE95E7EDCBD469BF9A8CE6EE0321AC1C77F (void);
// 0x00000095 System.Void Explosion/<Explode>d__3::System.IDisposable.Dispose()
extern void U3CExplodeU3Ed__3_System_IDisposable_Dispose_m6323FDA8CD6A90728B774930FBE5BE77024148E8 (void);
// 0x00000096 System.Boolean Explosion/<Explode>d__3::MoveNext()
extern void U3CExplodeU3Ed__3_MoveNext_m7FF575367596C970A277CCA7BB7B3D994F32DADC (void);
// 0x00000097 System.Object Explosion/<Explode>d__3::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CExplodeU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m86DF7C5A40A55D07317D9A2E9F52DC0BCB4C7D02 (void);
// 0x00000098 System.Void Explosion/<Explode>d__3::System.Collections.IEnumerator.Reset()
extern void U3CExplodeU3Ed__3_System_Collections_IEnumerator_Reset_m487EB8D5E237996B6DEF819E4290C05128D890D7 (void);
// 0x00000099 System.Object Explosion/<Explode>d__3::System.Collections.IEnumerator.get_Current()
extern void U3CExplodeU3Ed__3_System_Collections_IEnumerator_get_Current_mFD18D2F6818A63E397EFE2758F218DABF2B2F2CB (void);
// 0x0000009A Stick/stck Stick::GetStck()
extern void Stick_GetStck_m7A49B714FF83A4D429439940FB6990888867BD7B (void);
// 0x0000009B System.Void PauseScene::CheckPauseGame()
extern void PauseScene_CheckPauseGame_m62F66798AAE6E11CE0BC1647A0070E1DD4D42C62 (void);
// 0x0000009C System.Void PauseScene::PauseGame()
extern void PauseScene_PauseGame_m1F37173479C2FA938329D1B231A4B77C941D3506 (void);
// 0x0000009D System.Void PauseScene::ResumeGame()
extern void PauseScene_ResumeGame_m0F9D754B1CF8C903CA2B1769B1E8CD53021A1598 (void);
// 0x0000009E System.Void PauseScene::.ctor()
extern void PauseScene__ctor_m1935CE7512054B8C32181B546DCE99866AA3DB96 (void);
// 0x0000009F System.Void PauseScene::.cctor()
extern void PauseScene__cctor_m6D4D09CE812D1C8900573BC157A6EBB644735F2A (void);
// 0x000000A0 System.Void ControlShip::Start()
extern void ControlShip_Start_m5B912D419F20ED9BAFF9C7E0981D6ECF66DBA9F8 (void);
// 0x000000A1 System.Void ControlShip::LateUpdate()
extern void ControlShip_LateUpdate_m8960B65ECF9A86E6205A72BCA648E6A8BF4EDF7D (void);
// 0x000000A2 System.Void ControlShip::AnimateMotor()
extern void ControlShip_AnimateMotor_m95E9E12E5AB0068BB68740D04C32C91B610BAA27 (void);
// 0x000000A3 System.Collections.IEnumerator ControlShip::Shoot()
extern void ControlShip_Shoot_mC367083E502F3FB24057683528873F3C1ABE49FA (void);
// 0x000000A4 System.Void ControlShip::CallShield()
extern void ControlShip_CallShield_m981741EAE83CD99702ECA0E88C92115E68E6CA0C (void);
// 0x000000A5 System.Void ControlShip::OnCollisionEnter2D(UnityEngine.Collision2D)
extern void ControlShip_OnCollisionEnter2D_mF0FEC752B221AC776F36E9EB533C3AAD666FBC73 (void);
// 0x000000A6 System.Void ControlShip::.ctor()
extern void ControlShip__ctor_m1DFE695CD1EAB1B2CB9B079E3ED024FF986CFC93 (void);
// 0x000000A7 System.Void ControlShip/<Shoot>d__14::.ctor(System.Int32)
extern void U3CShootU3Ed__14__ctor_m918212AD71D0FFDF17E1552174E7421E325CE1D9 (void);
// 0x000000A8 System.Void ControlShip/<Shoot>d__14::System.IDisposable.Dispose()
extern void U3CShootU3Ed__14_System_IDisposable_Dispose_m93F9FD34F6D7228B444A2EA5F811CBEA0677F37F (void);
// 0x000000A9 System.Boolean ControlShip/<Shoot>d__14::MoveNext()
extern void U3CShootU3Ed__14_MoveNext_m51766104A83259371BBD8F8DBF6FA6EF3D2A4FB2 (void);
// 0x000000AA System.Object ControlShip/<Shoot>d__14::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CShootU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m73A3D6FF335CE4DB189C51BA32DBB48E4DC39194 (void);
// 0x000000AB System.Void ControlShip/<Shoot>d__14::System.Collections.IEnumerator.Reset()
extern void U3CShootU3Ed__14_System_Collections_IEnumerator_Reset_mAC9A39E29B95782473059C03DAACCA94530B5BD8 (void);
// 0x000000AC System.Object ControlShip/<Shoot>d__14::System.Collections.IEnumerator.get_Current()
extern void U3CShootU3Ed__14_System_Collections_IEnumerator_get_Current_m240B538B60BA691A5D8718C44313F9C7474A2C71 (void);
// 0x000000AD System.Void Flux.ReadMe::.ctor()
extern void ReadMe__ctor_m8FF25B75AB71F389F93E1E2A4DE7D2985ADD6F56 (void);
// 0x000000AE System.Void Gaminho.Level::.ctor()
extern void Level__ctor_mADE87462A9D13B244DC3F73F22BBA57A3CF8ADD9 (void);
// 0x000000AF System.Void Gaminho.ScenarioLimits::.ctor()
extern void ScenarioLimits__ctor_mE7ED76B4AD2650003AE4F5A075F9D7F03360730B (void);
// 0x000000B0 System.Void Gaminho.Shot::.ctor()
extern void Shot__ctor_m4BFE80FB650FFA47A20F832227B0A5F9BD84C4B3 (void);
// 0x000000B1 UnityEngine.Quaternion Gaminho.Statics::FaceObject(UnityEngine.Vector2,UnityEngine.Vector2,Gaminho.Statics/FacingDirection)
extern void Statics_FaceObject_mD7C4A0B189730DDA2CC972CE3204E2FE896FAF50 (void);
// 0x000000B2 System.Void Gaminho.Statics::.cctor()
extern void Statics__cctor_mB0BED0D7AA2F076698019E572C2CB7E7FEFC3BA4 (void);
static Il2CppMethodPointer s_methodPointers[178] = 
{
	Life_TakesLife_m432769A0DBB59D454784B3402F19D358B66CBDBD,
	Life_OnTriggerEnter2D_mB5E33226D7DB91144ACC54786FDF08388B017178,
	Life__ctor_m7EA475E113F8E58CDBD7F64DE7C3378510DD7926,
	ControlGame_Start_m41EDFA823653EE35B75C9E396B158F9C47330225,
	ControlGame_Update_m75CA34A327D9AE3F5BE315C068BB2D3946985B50,
	ControlGame_PauseMusic_m16FCABBE2805B4A9C3BFF4D691919B61104381E3,
	ControlGame_ContinueMusic_mF0FCA6D12DB9684795658C1C4FF1BC3ACDDA4EEE,
	ControlGame_EnablePauseWindow_mC87BB414060C0AD7214F136AEE360212385932A2,
	ControlGame_DisablePauseWindow_mF81275A2C40F059BF45495640DFC05A2E629BD4A,
	ControlGame_LevelPassed_m05B6235DD8F835987CA547397293A735BB6F6534,
	ControlGame_GameOver_m1E16954A2EB5306CA5D7959354BD4728C0321A13,
	ControlGame_Clear_m0681179893AC0309B4E59F5EA1033BE470C10074,
	ControlGame__ctor_mBB7334A9B707AA5AB4EC6F7C853BE92433BB4D4D,
	ControlStart_Start_m201D1963F24351E276FC706916096DE032376F51,
	ControlStart_StartClick_m4F919D6E668B781C8C5DC0EB2EB21611D882EFAB,
	ControlStart_ContinueClick_mBD000E184A0B18DFBA59C7536536E203CD989867,
	ControlStart_Quit_mD819D0CAD1831F9E534C96E210F5FFFF9FDB1747,
	ControlStart__ctor_mB1F89FE5B8F09B1F79FC79EA5BFDBDCEBEF790EA,
	EnemyControl_Start_mB2709A00C33F64E801FB336D9224A48BAEDF451F,
	EnemyControl_Process_m3026B4978A8349BD60037F2B25AFA4539B70FB7B,
	EnemyControl_EnemyCreate_m1DBC2B3B2C19C6C6D410EAB1360BB814DD12FD3E,
	EnemyControl_CallBoss_mEE1F75478CABCE55BC6A16FA00D0F843E7A58101,
	EnemyControl__ctor_mE15FA6B97AEFE883D943CC0D237897F435BC34BF,
	U3CProcessU3Ed__4__ctor_mA6DCD4EAD0188CA4C4AD0A5EF0D2A9FC2F8ADC15,
	U3CProcessU3Ed__4_System_IDisposable_Dispose_m382FC3C37C2241AC4E64B8D0EC2C821F916C3DE2,
	U3CProcessU3Ed__4_MoveNext_m3A08B74C2B6D96C327D949D051A8EA8D5B423DDC,
	U3CProcessU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m277414C234EE05878133959D21839B5949AC295B,
	U3CProcessU3Ed__4_System_Collections_IEnumerator_Reset_mE513AF299BB9BF436B27408FF4C0558A73F4676D,
	U3CProcessU3Ed__4_System_Collections_IEnumerator_get_Current_m158EF3608628631AC8F9A0A6CE2568AEB6566996,
	U3CCallBossU3Ed__6__ctor_mDA19F5EC5865BAF920AD4E709D8ADE95E743F363,
	U3CCallBossU3Ed__6_System_IDisposable_Dispose_m68D5010133251BF57F27F51D1BB928B1E4C8DDD9,
	U3CCallBossU3Ed__6_MoveNext_mDF3544E995D32004659624A65FB35CCD76C0B909,
	U3CCallBossU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2297F4117C95DDE24CFD237A355224230FA00BD9,
	U3CCallBossU3Ed__6_System_Collections_IEnumerator_Reset_mAB176BC3F3DE4AD36A0EAF25D234733D5CA47B3D,
	U3CCallBossU3Ed__6_System_Collections_IEnumerator_get_Current_mB515EAE6DED7E678D67D764943739ADEF1C84D27,
	RecordControl_Start_m8F7A53F239DE398E63E55AA2A9EF390392EB516F,
	RecordControl_UpdateName_m3C7C5ABFF677519820B6F281AF99A768064360DC,
	RecordControl__ctor_m8B9BD294AC3D1EDA3869E09F7EFC58F7B97304C4,
	AvoidPlayerShoot_Update_m9F04CC7942F3564D7D6D767688B1085737082DE0,
	AvoidPlayerShoot_OnTriggerStay2D_m11A3D1907CABECF03E64055F6D74EAC320B8F93A,
	AvoidPlayerShoot_StopMovingRightRoutine_m1270F8255ABDE3EF3F9345B44D7199C5A2DD1E8C,
	AvoidPlayerShoot__ctor_m2F72033E8F0B0C9A69210A0239507FAA88D97B05,
	U3CStopMovingRightRoutineU3Ed__4__ctor_mC9E973AD034D3BDAF7C2A6D99C47B0ABF62B1A27,
	U3CStopMovingRightRoutineU3Ed__4_System_IDisposable_Dispose_mEB6B7FE7825B7B23E4700FE655EC0119ACF7CF81,
	U3CStopMovingRightRoutineU3Ed__4_MoveNext_mB25589E8D5E9A567F1326D8113BB87EF4D0EB2B1,
	U3CStopMovingRightRoutineU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6FD7E48346DDC6DB25D8000EBA117799B428AD73,
	U3CStopMovingRightRoutineU3Ed__4_System_Collections_IEnumerator_Reset_m33895CB0E0464291042ECB278CF9FB89845E74E5,
	U3CStopMovingRightRoutineU3Ed__4_System_Collections_IEnumerator_get_Current_mFDFA2C26A33A873B1930730FC06967FCA3100825,
	Boss2_Start_mF5292CA924E8DB443334DB78CCD7201F768E15C8,
	Boss2_ShowParts_mCDC314F0FBDB8D208165FAAE3F8F4D358F7FD315,
	Boss2_Update_m8CE583E0DAC32C0A374F69542AD8C054754C0C14,
	Boss2_Shot_m35B8B00F7AEF10F996EF1D55BEF3575E5BAFF3D6,
	Boss2_GetOneActive_m41CEB8C5D4F936E741F9C9F684F40EAE7D3DADA3,
	Boss2_KillMe_mDE3ED52B5D597F8CEB197D98E1F15E9334C71285,
	Boss2__ctor_mAF0CEE4D38FEAFC1DE100B0F2E0701E06B14991F,
	U3CShowPartsU3Ed__5__ctor_mBDD0E8898D91E138B7042D22F0A5E1456EA501C8,
	U3CShowPartsU3Ed__5_System_IDisposable_Dispose_m0AC2D2E39174DA2D173BDA8759091EEE1D71F146,
	U3CShowPartsU3Ed__5_MoveNext_m0B40D4DCC0A1EBAC248878B225A64D4925E31704,
	U3CShowPartsU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4F4521C1346516C77ED9C7B550671F928987CE41,
	U3CShowPartsU3Ed__5_System_Collections_IEnumerator_Reset_m0468FE7B0426062B26C538D23C7C5FF8CC38570D,
	U3CShowPartsU3Ed__5_System_Collections_IEnumerator_get_Current_m20F3D7FA705381FA5AA9C2B5C037BDCB2BC2128A,
	U3CShotU3Ed__7__ctor_mE5DADDE0420441F86985C44BD718DD08A4AD334C,
	U3CShotU3Ed__7_System_IDisposable_Dispose_m8F588491D89FF21288555692FE3A90E505E341F7,
	U3CShotU3Ed__7_MoveNext_m6010D4868EF31AE0774066824F01233F5F37D4AE,
	U3CShotU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m12B604A9AC558372662A0FFD180A325ACDD91EA6,
	U3CShotU3Ed__7_System_Collections_IEnumerator_Reset_mE289E17958922FDDCC8DE7C1FA61940025274E62,
	U3CShotU3Ed__7_System_Collections_IEnumerator_get_Current_m935EF7A6B9356B060AA9DFD3E78AA608C890BC25,
	Boss3_Start_m025FF1DBDC22B6304FBDBAD41E2A028B5BCF4E2C,
	Boss3_ShowParts_m237CC1FA5AE12E3DC8B0F2E864759C88F37E1EC6,
	Boss3_AttackNow_m751CAFCD3CD70F4748E1BE4A61688B418BF83420,
	Boss3_Attack_mBC827254E14F721046A48C74FBDCE480D7EB8006,
	Boss3_GetOneActive_mC454EE4919EB35E3CADF35A2C047B267CBB5754F,
	Boss3_KillMe_m485F8403E9462C6F16E4A1E1346373AE8AE15029,
	Boss3__ctor_m643E6AD2933C0D491F1706E2F4CA3BA2F3820306,
	U3CShowPartsU3Ed__5__ctor_mE02D1048C34075267CA9F8A9AAA9BAA01A251D2D,
	U3CShowPartsU3Ed__5_System_IDisposable_Dispose_mFFAB94361C876791A1720E45B41DDE4920D6E962,
	U3CShowPartsU3Ed__5_MoveNext_mCD5696204CDF2A5ECBC56EF51E055A070AC9FB77,
	U3CShowPartsU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m46AC63FF765EC7ABFD50ABD1A0EC06666CCE0B0F,
	U3CShowPartsU3Ed__5_System_Collections_IEnumerator_Reset_m6B70E7B67CF4CDCB8A4933138894ADD37C944524,
	U3CShowPartsU3Ed__5_System_Collections_IEnumerator_get_Current_mE6AF45BEC418417DB98C019E25CDB0370CB836FF,
	U3CAttackNowU3Ed__6__ctor_m807B06176433FA7EB7E91A0C957DA1F3903A1BB9,
	U3CAttackNowU3Ed__6_System_IDisposable_Dispose_m084196477A3C10B1664186E740CEE59FD3B30521,
	U3CAttackNowU3Ed__6_MoveNext_mEA7C3F8A7C4FA8E6D4387CE9F9A6709AFEB394E7,
	U3CAttackNowU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m340352050CC94EAE9A072030CD314DA56DD329AA,
	U3CAttackNowU3Ed__6_System_Collections_IEnumerator_Reset_m9E1C7C0E00159B453D194492ECF021B8BB7CD45A,
	U3CAttackNowU3Ed__6_System_Collections_IEnumerator_get_Current_m574DD65751CE17C59EB1B1B4A52EC305243484D0,
	U3CAttackU3Ed__7__ctor_m1E3D32EDA81B13F2EE38F2859D3B2C4F28E9D327,
	U3CAttackU3Ed__7_System_IDisposable_Dispose_m6A015A6CA5CCBB4708004CC3651BB9B1D3725CFF,
	U3CAttackU3Ed__7_MoveNext_mA361D488087872EB4D071C00B7A2E97B943ED301,
	U3CAttackU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF28BAAACFCDE294FEDE76B40F09FF75F8FC16E52,
	U3CAttackU3Ed__7_System_Collections_IEnumerator_Reset_m5A5C9CB773069AF2AEB75EA9A0A398A9459D72B7,
	U3CAttackU3Ed__7_System_Collections_IEnumerator_get_Current_m52CBBA00FF612CAD09F60DF816EAC8E2A237414F,
	Enemy_Start_m9FA35B427F2B9FDFD390E9812C2556775C62CB02,
	Enemy_Update_mA01EE7AF5D3B97687752E9D22BECB4A3E13F8FD2,
	Enemy_OnCollisionEnter2D_m47EAF0D1D60EC5BF3953975EA6BF15189DF82A12,
	Enemy_MyDeath_mA4085ED4BD10E6B3EDEE186807D3C8FB762A4CC8,
	Enemy_KillMe_m24D0CC29C673187902BFD4554072865ADF3F2EB5,
	Enemy_Create_m66ACA885B980722A913D038190E87E6AFFA55759,
	Enemy_Shoot_m9698649D8FCE4A907B5EBAA30105E3DF881FB767,
	Enemy_EndBoss_mA358579DEF2D3DAF68CD80B9AD4C96C1981554DA,
	Enemy_PStick_m5BAC91E00684770270D3B5164328B2EF020D9173,
	Enemy__ctor_m3C82F8269DE4132408E15B523907244771640734,
	U3CKillMeU3Ed__10__ctor_mCBE3F42CF5E6BEB5E774F037B0444C559C24EC4C,
	U3CKillMeU3Ed__10_System_IDisposable_Dispose_m2DB73B1C1F038ACD13017517E648F7BC34FE75A7,
	U3CKillMeU3Ed__10_MoveNext_m2B827634D8CEADE16F1ABA524332C1D91AAEEA08,
	U3CKillMeU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB162D3E0999042813FBE550CD7FD7926C498BF3C,
	U3CKillMeU3Ed__10_System_Collections_IEnumerator_Reset_m144E9C6610E020F7905EF9E9E55E6D769047778C,
	U3CKillMeU3Ed__10_System_Collections_IEnumerator_get_Current_m89F0A6CE9070973A816212BD6572DA6DC9D9F303,
	U3CShootU3Ed__12__ctor_m4884E34148F61D16DCBE9E6C9E29CF766636E036,
	U3CShootU3Ed__12_System_IDisposable_Dispose_m9E7ADDC293233521A708C4B1114000B969F4F6E4,
	U3CShootU3Ed__12_MoveNext_mD258E3C08A9B604CFA4F85EFA5B6E32189C9EC55,
	U3CShootU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA3E2421A685652897F0F5F6D05DA25B8ACDEE5DD,
	U3CShootU3Ed__12_System_Collections_IEnumerator_Reset_m5C552696876E029CE03DD55B746CD7422330D2DF,
	U3CShootU3Ed__12_System_Collections_IEnumerator_get_Current_m9D53A9EAB026124E6E8AF1E6BC87C3375A604866,
	ItemDrop_OnTriggerEnter2D_m1A1B2CBD8DABD7C2C8C666248C9E7B9A56F022A7,
	ItemDrop__ctor_m5D826B2329C00417D6FB12D4C40DE3EB0DB55AF2,
	GameEvent_Raise_m9B5D84B86942CF2BFF6B064CFBE740D14AC41169,
	GameEvent_RegisterListener_mCF8253B702F145F689525B4DFD266F14259941DE,
	GameEvent_UnregisterListener_mA64E767AE34F712A1D506DC25037A57302DCAF59,
	GameEvent__ctor_m8337DB39CF91F554DDF9F08F43DD8093CDF3D362,
	GameEventListener_OnEnable_mED6474898A6BDA74E5901EC4F92179E5A743EABF,
	GameEventListener_OnDisable_m294B8AD260F889DC6315ABBC3708388BC69D0FB2,
	GameEventListener_OnEventRaised_m64F601143F91C20869FE7D570F8C4530606AE873,
	GameEventListener__ctor_m59096273CAC9367011CA8BFD14CCBA3E3A4CC8AD,
	GameSave_Awake_m8FDE53993AECC9CA915BFD62E86F10EA2AB272FC,
	GameSave_Start_m0A5DC52CFF75277784AB9A1E9CE44532AB0023A1,
	GameSave_LoadSaveData_mA0E87179E2A403812A58E5BB704981C7EC2B3E17,
	GameSave__ctor_mC4C7321C5218614930AB431A5E1254608C3F4EA8,
	GameSaveData__ctor_m558C28225F9028417ADA7AC2A15D1702A6F2788C,
	GameSaveDatas__ctor_m2F11588F7657D30EBEB87E30ECE51BE6797D92DB,
	FollowPlayer_Awake_mC670530149DD0AF283FBD985226E690947E9BA2B,
	FollowPlayer_LateUpdate_m15E369E8794A1195493D5B2C5E0183739DCB8EC0,
	FollowPlayer__ctor_mE71062263690B516F460CF232D5144C1C85017AA,
	CallScene_Call_m8CDF9D77505FAEDF29068B890862D09654434CCE,
	CallScene__ctor_m6B52AD5FB87324D006A0BFF36D122D8E5096AE9C,
	DestroyTime_Start_m8C0A193A37746E7A2A2C617E998A31E724373497,
	DestroyTime_CallDestroy_m61D5F2B05283C2474DC66285AC88FDA79B8E1BD6,
	DestroyTime__ctor_mAB5E312F1EAAD1B3B69CC64F2B95B607F8EE4F88,
	U3CCallDestroyU3Ed__2__ctor_m7A7A8DC06CB12FA979DC587DA59CD00B21710FAF,
	U3CCallDestroyU3Ed__2_System_IDisposable_Dispose_m93E09EB5836B606D9F59C3CB667BDD2C8F1215BD,
	U3CCallDestroyU3Ed__2_MoveNext_mCDEF6B443D6ED1BA89399F05AEC81423D64C107B,
	U3CCallDestroyU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA97E34B5CF7FEDA8B2A9EFBCE21A197CE51A2BB2,
	U3CCallDestroyU3Ed__2_System_Collections_IEnumerator_Reset_mEAC272124FCDF01C2C344D034F9CBA6F076E2EF9,
	U3CCallDestroyU3Ed__2_System_Collections_IEnumerator_get_Current_m89A18098EC6562CFAFAD044268F750C304307BA0,
	Explosion_Start_m519BD45EC393F52D86FB64B10889D5CBADCF4C22,
	Explosion_Explode_m0586F5F99D95A44520E522FA9CF6256DCB7FC258,
	Explosion__ctor_m1400515C43124E852380BB8283E15042AF0A5094,
	U3CExplodeU3Ed__3__ctor_mA6402CE95E7EDCBD469BF9A8CE6EE0321AC1C77F,
	U3CExplodeU3Ed__3_System_IDisposable_Dispose_m6323FDA8CD6A90728B774930FBE5BE77024148E8,
	U3CExplodeU3Ed__3_MoveNext_m7FF575367596C970A277CCA7BB7B3D994F32DADC,
	U3CExplodeU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m86DF7C5A40A55D07317D9A2E9F52DC0BCB4C7D02,
	U3CExplodeU3Ed__3_System_Collections_IEnumerator_Reset_m487EB8D5E237996B6DEF819E4290C05128D890D7,
	U3CExplodeU3Ed__3_System_Collections_IEnumerator_get_Current_mFD18D2F6818A63E397EFE2758F218DABF2B2F2CB,
	Stick_GetStck_m7A49B714FF83A4D429439940FB6990888867BD7B,
	PauseScene_CheckPauseGame_m62F66798AAE6E11CE0BC1647A0070E1DD4D42C62,
	PauseScene_PauseGame_m1F37173479C2FA938329D1B231A4B77C941D3506,
	PauseScene_ResumeGame_m0F9D754B1CF8C903CA2B1769B1E8CD53021A1598,
	PauseScene__ctor_m1935CE7512054B8C32181B546DCE99866AA3DB96,
	PauseScene__cctor_m6D4D09CE812D1C8900573BC157A6EBB644735F2A,
	ControlShip_Start_m5B912D419F20ED9BAFF9C7E0981D6ECF66DBA9F8,
	ControlShip_LateUpdate_m8960B65ECF9A86E6205A72BCA648E6A8BF4EDF7D,
	ControlShip_AnimateMotor_m95E9E12E5AB0068BB68740D04C32C91B610BAA27,
	ControlShip_Shoot_mC367083E502F3FB24057683528873F3C1ABE49FA,
	ControlShip_CallShield_m981741EAE83CD99702ECA0E88C92115E68E6CA0C,
	ControlShip_OnCollisionEnter2D_mF0FEC752B221AC776F36E9EB533C3AAD666FBC73,
	ControlShip__ctor_m1DFE695CD1EAB1B2CB9B079E3ED024FF986CFC93,
	U3CShootU3Ed__14__ctor_m918212AD71D0FFDF17E1552174E7421E325CE1D9,
	U3CShootU3Ed__14_System_IDisposable_Dispose_m93F9FD34F6D7228B444A2EA5F811CBEA0677F37F,
	U3CShootU3Ed__14_MoveNext_m51766104A83259371BBD8F8DBF6FA6EF3D2A4FB2,
	U3CShootU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m73A3D6FF335CE4DB189C51BA32DBB48E4DC39194,
	U3CShootU3Ed__14_System_Collections_IEnumerator_Reset_mAC9A39E29B95782473059C03DAACCA94530B5BD8,
	U3CShootU3Ed__14_System_Collections_IEnumerator_get_Current_m240B538B60BA691A5D8718C44313F9C7474A2C71,
	ReadMe__ctor_m8FF25B75AB71F389F93E1E2A4DE7D2985ADD6F56,
	Level__ctor_mADE87462A9D13B244DC3F73F22BBA57A3CF8ADD9,
	ScenarioLimits__ctor_mE7ED76B4AD2650003AE4F5A075F9D7F03360730B,
	Shot__ctor_m4BFE80FB650FFA47A20F832227B0A5F9BD84C4B3,
	Statics_FaceObject_mD7C4A0B189730DDA2CC972CE3204E2FE896FAF50,
	Statics__cctor_mB0BED0D7AA2F076698019E572C2CB7E7FEFC3BA4,
};
static const int32_t s_InvokerIndices[178] = 
{
	1369,
	1380,
	1614,
	1614,
	1614,
	1614,
	1614,
	1614,
	1614,
	1614,
	1614,
	1614,
	1614,
	1614,
	1614,
	1614,
	1614,
	1614,
	1614,
	1573,
	1614,
	1573,
	1614,
	1369,
	1614,
	1596,
	1573,
	1614,
	1573,
	1369,
	1614,
	1596,
	1573,
	1614,
	1573,
	1614,
	1614,
	1614,
	1614,
	1380,
	1573,
	1614,
	1369,
	1614,
	1596,
	1573,
	1614,
	1573,
	1614,
	1573,
	1614,
	1573,
	1573,
	1380,
	1614,
	1369,
	1614,
	1596,
	1573,
	1614,
	1573,
	1369,
	1614,
	1596,
	1573,
	1614,
	1573,
	1614,
	1114,
	1573,
	1113,
	1573,
	1380,
	1614,
	1369,
	1614,
	1596,
	1573,
	1614,
	1573,
	1369,
	1614,
	1596,
	1573,
	1614,
	1573,
	1369,
	1614,
	1596,
	1573,
	1614,
	1573,
	1614,
	1614,
	1380,
	1614,
	1573,
	1369,
	1573,
	1614,
	1614,
	1614,
	1369,
	1614,
	1596,
	1573,
	1614,
	1573,
	1369,
	1614,
	1596,
	1573,
	1614,
	1573,
	1380,
	1614,
	1614,
	1380,
	1380,
	1614,
	1614,
	1614,
	1614,
	1614,
	1614,
	1614,
	1614,
	1614,
	878,
	1614,
	1614,
	1614,
	1614,
	1380,
	1614,
	1614,
	1573,
	1614,
	1369,
	1614,
	1596,
	1573,
	1614,
	1573,
	1614,
	1573,
	1614,
	1369,
	1614,
	1596,
	1573,
	1614,
	1573,
	2526,
	1614,
	1614,
	1614,
	1614,
	2547,
	1614,
	1614,
	1614,
	1573,
	1614,
	1380,
	1614,
	1369,
	1614,
	1596,
	1573,
	1614,
	1573,
	1614,
	1614,
	1614,
	1614,
	2053,
	2547,
};
extern const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	178,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_AssemblyU2DCSharp_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
