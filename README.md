# FluxGamesTasks

- Increase enemy's shooting:

Increased number multiplied by shot.SpeedShooter in Enemy.cs.

- Enemy always dodge the shot

Created and attached script AvoidPlayerShoot.cs to enemy prefab "En5". It is basically triggered by 
the Ship's ShootTrajectory area and move out of the way.

- Create Save

Reads the Json file playerOneSaveData and interpret it as an GameSaveData in the GameSave.cs. Store player and level
in playersSaveData[index].player and playersSaveData[index].level respectively. Replaces Statics.CurrentLevel
to Load and Save when its needed in game.

- Create Ghost P2

Created another ship and attached the script FollowPlayer.cs. It creates a list of positions and rotations from player.
After bigger than followDistance and rotationDelay, it just replicates the coordinates on those lists.

- Add Life Shield UI Feedback

Similar as BarLife works. Referenced as Bar Shield in ControlGame.cs.

- Add Pause

Used Scriptable Object Events to raise event and ControlGame.cs handles it. It pauses music and enable pause window.

- Build UWP

Commented lines that were causing building errors. UWP and Windows builds availables.


