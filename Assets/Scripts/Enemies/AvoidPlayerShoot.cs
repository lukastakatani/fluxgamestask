using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AvoidPlayerShoot : MonoBehaviour
{
    [SerializeField]
    private bool _isAvoidActive = true, _isMoveRight = false;

    private void Update()
    {
        if (_isMoveRight && _isAvoidActive)
        {
           transform.Translate(Vector3.right * 750 * Time.deltaTime);
        }
    }

    private void OnTriggerStay2D(Collider2D other)
    {
        if (other.gameObject.tag == "ShootTrajectory")
        {
            _isMoveRight = true;
            StartCoroutine(StopMovingRightRoutine());
        }
    }

    IEnumerator StopMovingRightRoutine()
    {
        yield return new WaitForSeconds(.3f);
        _isMoveRight = false;
    }
}
