using UnityEngine;
using System.Collections.Generic;

public class FollowPlayer : MonoBehaviour
{

    public GameObject player;
    public int followDistance, rotationDelay;
    private List<Vector3> storedPositions;
    private List<Quaternion> storedRotations;


    void Awake()
    {
        //create a blank list
        storedPositions = new List<Vector3>(); 
        storedRotations = new List<Quaternion>(); 
    }

    void LateUpdate()
    {
        Vector3 playerPos = player.GetComponent<RectTransform>().position;
        Quaternion playerRot = player.GetComponent<RectTransform>().rotation;

        // Position
        if (storedPositions.Count == 0) //check if the list is empty
        {
            storedPositions.Add(playerPos); //store the players currect position
            return;
        }
        else if (storedPositions[storedPositions.Count - 1] != playerPos)
        {
            storedPositions.Add(playerPos); //store the position every frame
        }
        else if (GetComponent<RectTransform>().position.y != playerPos.y) //check if the ally is airborne and correct it if necessary
        {
            storedPositions.Add(playerPos);
        }

        if (storedPositions.Count > followDistance)
        {
            GetComponent<RectTransform>().position = storedPositions[0]; //move
            storedPositions.RemoveAt(0); //delete the position that player just move to
        }

        // Rotation
        if (storedRotations.Count == 0) 
        {
            storedRotations.Add(playerRot); //store the players currect rotation
            return;
        }
        else if (storedRotations[storedRotations.Count - 1] != playerRot)
        {
            storedRotations.Add(playerRot); //store the rotation every frame
        }
        else if (GetComponent<RectTransform>().rotation != playerRot) //check if the ally is airborne and correct it if necessary
        {
            storedRotations.Add(playerRot);
        }

        //Debug.Log(storedRotations.Count);

        if (storedRotations.Count > rotationDelay)
        {
            GetComponent<RectTransform>().rotation = storedRotations[0]; //rotate
            storedRotations.RemoveAt(0); //delete the rotation that player just move to
        }
    }
}