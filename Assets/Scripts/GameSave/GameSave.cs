using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class GameSave : MonoBehaviour
{
    private string path, jsonString;

    public GameSaveDatas playersDatas = new GameSaveDatas();

    // Singleton
    public static GameSave instance;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }

        DontDestroyOnLoad(this.gameObject);
    }

    void Start()
    {
        playersDatas.playersSaveData.Add(new GameSaveData("taka", 0));
        Debug.Log("EMPTY Player: " + playersDatas.playersSaveData[0].player + " Level: " +
                playersDatas.playersSaveData[0].level.ToString());

        path = System.IO.Directory.GetCurrentDirectory() + "\\" + "playerOneSaveData.json";
        Debug.Log(path);

        jsonString = File.ReadAllText(path);
        //Debug.Log(jsonString);

    }

    public void LoadSaveData()
    {
        playersDatas = JsonUtility.FromJson<GameSaveDatas>(jsonString);
        for (int i = 0; i < playersDatas.playersSaveData.Count; i++)
        {
            Debug.Log("LOAD Player: " + playersDatas.playersSaveData[i].player + " Level: " +
                playersDatas.playersSaveData[i].level.ToString());
        }
    }
}
