using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class GameSaveData
{
    public string player;
    public int level;

    public GameSaveData(string player, int level)
    {
        this.player = player;
        this.level = level;
    }
}

[System.Serializable]
public class GameSaveDatas
{
    public List<GameSaveData> playersSaveData;
}
