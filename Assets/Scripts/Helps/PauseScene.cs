using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseScene : MonoBehaviour
{
    [SerializeField]
    private GameEvent onGamePaused, onGameContinue;
    static private bool isGamePaused = false;

    public void CheckPauseGame()
    {
        if (!isGamePaused)
        {
            PauseGame();
            isGamePaused = true;
        }
        else
        {
            ResumeGame();
            isGamePaused = false;
        }
        Debug.Log(isGamePaused);
    }

    private void PauseGame()
    {
        Time.timeScale = 0;
        onGamePaused.Raise();
    }

    private void ResumeGame()
    {
        Time.timeScale = 1;
        onGameContinue.Raise();
    }
}
