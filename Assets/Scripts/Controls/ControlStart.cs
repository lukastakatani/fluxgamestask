﻿using Gaminho;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ControlStart : MonoBehaviour {
    public Text Record;
	// Use this for initialization
	void Start () {
        //Reset the variables to start the game from scratch
        Statics.WithShield = true;
        Statics.EnemiesDead = 0;
        //Statics.CurrentLevel = 0;
        GameSave.instance.playersDatas.playersSaveData[0].level = 1;
        Debug.Log("START Player: " + GameSave.instance.playersDatas.playersSaveData[0].player + " Level: " +
                GameSave.instance.playersDatas.playersSaveData[0].level.ToString());
        Statics.Points = 0;
        Statics.ShootingSelected = 2;
        //Loads Record
        if (PlayerPrefs.GetInt(Statics.PLAYERPREF_VALUE) == 0)
        {
            PlayerPrefs.SetString(Statics.PLAYERPREF_NEWRECORD, "Nobody");
        }
        Record.text = "Record: " + PlayerPrefs.GetString(Statics.PLAYERPREF_NEWRECORD) + "(" + PlayerPrefs.GetInt(Statics.PLAYERPREF_VALUE) + ")";
       
	}

	public void StartClick()
    {
        GetComponent<AudioSource>().Stop();
        GameObject.Instantiate(Resources.Load(Statics.PREFAB_HISTORY) as GameObject);
    }

    public void ContinueClick()
    {
        GetComponent<AudioSource>().Stop();
        GameSave.instance.LoadSaveData();
    }

    public void Quit()
    {
        Application.Quit();
    }

    
}
