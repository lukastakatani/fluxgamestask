﻿using Gaminho;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class ControlGame : MonoBehaviour {
    public ScenarioLimits ScenarioLimit;
    public Level[] Levels;
    public Image Background;
    [Header("UI")]
    public Text TextStart;
    public Text TextPoints;
    public Transform BarLife, BarShield;

    [SerializeField]
    private GameObject pauseWindow;
    [SerializeField]
    private ControlShip ship;
    
    // Use this for initialization
    void Start () {
        Statics.EnemiesDead = 0;
        //Background.sprite = Levels[Statics.CurrentLevel].Background;
        Background.sprite = Levels[GameSave.instance.playersDatas.playersSaveData[0].level-1].Background;
        //TextStart.text = "Stage " + (Statics.CurrentLevel + 1);
        TextStart.text = "Stage " + (GameSave.instance.playersDatas.playersSaveData[0].level.ToString());
        //GetComponent<AudioSource>().PlayOneShot(Levels[Statics.CurrentLevel].AudioLvl);
        GetComponent<AudioSource>().PlayOneShot(Levels[GameSave.instance.playersDatas.playersSaveData[0].level-1].AudioLvl);

    }

    private void Update()
    {
        TextPoints.text = Statics.Points.ToString();
        BarLife.localScale = new Vector3(Statics.Life / 10f, 1, 1);
        BarShield.localScale = new Vector3(ship.lifeShield / 10f, 1, 1);
    }

    public void PauseMusic()
    {
        GetComponent<AudioSource>().Stop();
    }

    public void ContinueMusic()
    {
        //GetComponent<AudioSource>().PlayOneShot(Levels[Statics.CurrentLevel].AudioLvl);
        GetComponent<AudioSource>().PlayOneShot(Levels[GameSave.instance.playersDatas.playersSaveData[0].level - 1].AudioLvl);
    }

    public void EnablePauseWindow()
    {
        pauseWindow.SetActive(true);
    }

    public void DisablePauseWindow()
    {
        pauseWindow.SetActive(false);
    }

    public void LevelPassed()
    {
        
        Clear();
        //Statics.CurrentLevel++;
        GameSave.instance.playersDatas.playersSaveData[0].level = GameSave.instance.playersDatas.playersSaveData[0].level + 1;
        string json = JsonUtility.ToJson(GameSave.instance.playersDatas);
        File.WriteAllText(System.IO.Directory.GetCurrentDirectory() + "\\" + "playerOneSaveData.json", json);
        //Statics.Points += 1000 * Statics.CurrentLevel;
        Statics.Points += 1000 * GameSave.instance.playersDatas.playersSaveData[0].level-1;
        if (/*Statics.CurrentLevel*/ (GameSave.instance.playersDatas.playersSaveData[0].level-1) < 3)
        {
            GameObject.Instantiate(Resources.Load(Statics.PREFAB_LEVELUP) as GameObject);
        }
        else
        {
            GameObject.Instantiate(Resources.Load(Statics.PREFAB_CONGRATULATION) as GameObject);
        }
    }
    //Oops, when you lose (: Starts from Zero
    public void GameOver()
    {
        BarLife.localScale = new Vector3(0, 1, 1);
        BarShield.localScale = new Vector3(0, 1, 1);
        Clear();
        Destroy(Statics.Player.gameObject);
        GameObject.Instantiate(Resources.Load(Statics.PREFAB_GAMEOVER) as GameObject);
    }
    private void Clear()
    {
        GetComponent<AudioSource>().Stop();
        GameObject[] Enemies = GameObject.FindGameObjectsWithTag("Enemy");
        foreach (GameObject ini in Enemies)
        {
            Destroy(ini);
        }
    }
}
